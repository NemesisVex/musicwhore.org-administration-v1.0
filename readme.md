# Musicwhore.org Administration

Musicwhore.org Administration is a legacy application to maintain the Musicwhore.org Artist Database.

## History

Before becoming a music blog, Musicwhore.org was a webzine which included a database of artist information, particularly localized discographies of Japanese indie rock artists.

The database was first maintained on a custom-built system from 2000-2003. It was then migrated to CodeIgniter, then later to Laravel.

The application is no longer in use but remains online.

## Dependencies

* [guzzlehttp/guzzle](https://packagist.org/packages/guzzlehttp/guzzle)
* [exeu/apai-io](https://packagist.org/packages/exeu/apai-io)
* [mikealmond/musicbrainz](https://packagist.org/packages/mikealmond/musicbrainz)
* [ricbra/php-discogs-api](https://packagist.org/packages/ricbra/php-discogs-api)
* [vinelab/itunes](https://packagist.org/packages/vinelab/itunes)

## Laravel PHP Framework

[![Build Status](https://img.shields.io/travis/laravel/framework.svg)](https://travis-ci.org/laravel/framework)
[![Total Downloads](https://img.shields.io/packagist/dt/laravel/framework.svg)](https://packagist.org/packages/laravel/framework)
[![Latest Version](https://img.shields.io/github/tag/laravel/framework.svg)](https://github.com/laravel/framework/releases)
[![Dependency Status](https://www.versioneye.com/php/laravel:framework/badge.svg)](https://www.versioneye.com/php/laravel:framework)

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable, creative experience to be truly fulfilling. Laravel attempts to take the pain out of development by easing common tasks used in the majority of web projects, such as authentication, routing, sessions, and caching.

Laravel aims to make the development process a pleasing one for the developer without sacrificing application functionality. Happy developers make the best code. To this end, we've attempted to combine the very best of what we have seen in other web frameworks, including frameworks implemented in other languages, such as Ruby on Rails, ASP.NET MVC, and Sinatra.

Laravel is accessible, yet powerful, providing powerful tools needed for large, robust applications. A superb inversion of control container, expressive migration system, and tightly integrated unit testing support give you the tools you need to build any application with which you are tasked.

## Official Documentation

Documentation for the entire framework can be found on the [Laravel website](http://laravel.com/docs).

### Contributing To Laravel

**All issues and pull requests should be filed on the [laravel/framework](http://github.com/laravel/framework) repository.**

### License

The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)
